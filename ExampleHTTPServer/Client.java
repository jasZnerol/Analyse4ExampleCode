import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.InputStream;

import java.util.stream.Collectors;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;

public class Client {


    /*
     * Returns what is received by connecting to localhost:9997
     */
    public static String helloWorldRoute() {

        // Create an http-client which will send the request and handle the response
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:9997/"))
                .timeout(Duration.ofMinutes(1))
                .GET()
                .build();
        try {
            // Send the request, get the response and try to parse it into a string
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            return response.body();
        } catch(IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
    

    /*
     * Sends the requestMessage to localhost:9997/echo and returns the answer from the server.
     */
    public static String echoRoute(String requestMessage){

        // Create a http-client which will send the request and handle the response
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:9997/echo"))
                .timeout(Duration.ofMinutes(1))
                .POST(BodyPublishers.ofString(requestMessage))
                .build();
        try {
            // Send the request, get the response and try to parse it into a string
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            return response.body();
        } catch(IOException | InterruptedException e) {
            e.printStackTrace();
            // If an error occured during reading the response return null
            return null;
        } 
    }

    
    /*
     * Writes what is received by connecting to localhost:9997/fxml into a file called "received.fxml"
     */
    public static void fxmlRoute(){

        // Create a http-client which will send the request and handle the response
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:9997/fxml"))
                .timeout(Duration.ofMinutes(1))
                .GET()
                .build();
        try {

            // Send the request and get the response
            HttpResponse<InputStream> response = client.send(request, BodyHandlers.ofInputStream());
            InputStream body = response.body();

            // Prepare file for writing
            File file = new File("received.fxml");
            FileOutputStream fileContent = new FileOutputStream(file);

            // Write the response to the file
            body.transferTo(fileContent);

            // Clean up
            body.close();
            fileContent.close();

        } catch(IOException | InterruptedException e) {
            e.printStackTrace();
        } 
    }

    /*
     * Tests all implemented routes from the server
     */
    public static void main(String[] args){
        System.out.println(helloWorldRoute());
        System.out.println("**************************************************");
        System.out.println(echoRoute("Ich funktioniere!"));
        fxmlRoute();
    }
}