import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.File;

import java.util.stream.Collectors;

import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

public class Server {

	private HttpServer server;

	/**
	 * Creates a new server on port 9993. It can handle simple requests and
	 * showcases some features. You can reach all routes with either the Client.java
	 * file from java or even with your webbrowser.
	 */
	public Server() {
		try {

			// Creates a new HTTP server on port 9993.
			this.server = HttpServer.create(new InetSocketAddress(9997), 0);

			// Set execution mode to parallel. All requests are handled in their own thread
			server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());

			// Bind functions to specific URLs
			server.createContext("/", Server::rootRequests);
			server.createContext("/echo", Server::echoRequests);
			server.createContext("/fxml", Server::fxmlRequests);

			// Start server
			server.start();

		} catch (IOException e) {
			System.err.println("Could not create Server! Is another server running on port 9993?");
			System.exit(1);
		}
	}

	/**
	 * Function called to handle a request at / (eg. localhost:9997 or
	 * localhost:9997/). It is called for each request to /. This route always
	 * replies with "hello world".
	 * 
	 * @param exchange The request that will be handled.
	 */
	private static void rootRequests(HttpExchange exchange) {
		try {
			String response = "hello world";
			// Set status code 200 (OK) and content length for response
			exchange.sendResponseHeaders(200, response.getBytes("UTF-8").length);

			// Write the response to the client
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes("UTF-8"));
			os.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Function called to handle a request at /echo (localhost:9997/echo). It is
	 * called for each request to /echo. This route just echos what it gets to
	 * demonstrate how to read from a request.
	 * 
	 * @param exchange The request that will be handled.
	 */
	private static void echoRequests(HttpExchange exchange) {
		try {
			// The request body (the data that was send with the request)
			// comes as an input stream. We expect it to be a String so we parse it into a
			// String.
			// You could also interpret the input stream as other types, like a file or a
			// serialized object and parse it accordingly.
			// Everything that can be converted into a byte[] can be sent and received.
			String requestBody = null;
			try (BufferedReader rd = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))) {
				requestBody = rd.lines().collect(Collectors.joining("\n"));
			} catch (IOException e) {
				e.printStackTrace();
			}

			/*
			 * In real world applications you would process the body here.
			 */

			// Echo back what was read from the body
			String response = "Server received: \n" + requestBody;

			// Set status code 200 (OK) and content length for response
			exchange.sendResponseHeaders(200, response.getBytes("UTF-8").length);

			// Write the response to the client
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes("UTF-8"));
			os.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Function called to handle a request at /fxml (localhost:9997/fxml). It is
	 * called for each request to /fxml. This route reads a file named fxml.fxml
	 * replies with its content. The file can contain anything and might as well be
	 * a mp4 or zip since we are just forwarding all the bytes read.
	 * 
	 * @param exchange The request that will be handled.
	 */
	private static void fxmlRequests(HttpExchange exchange) {
		try {

			// Prepare file for reading
			File file = new File("fxml.fxml");
			InputStream fileContent = new FileInputStream(file);

			// Set status code 200 (OK) and content length for response
			exchange.sendResponseHeaders(200, file.length());

			// Write everything from the file to the request response.
			OutputStream os = exchange.getResponseBody();
			fileContent.transferTo(os);

			// Clean up
			os.close();
			fileContent.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Runs the server. As this is just an example no other method to stop the
	 * server than ctrl+c is implemented.
	 */
	public static void main(String[] args) {
		new Server();
	}
}